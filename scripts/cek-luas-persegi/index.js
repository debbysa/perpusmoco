// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

const luasPersegiShared = require("@mocobaas/shared/luas-persegi-shared");
// kapan import pake {} kapan enggak?
/**
 *
 * @description handler for cek-luas-persegi
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  let sisi = ctx.data.sisi;
  const luasPersegi = await luasPersegiShared(sisi);
  return {
    data: `luas persegi dengan sisi ${sisi} adalah ${luasPersegi}`,
    error: null,
  };
}

module.exports = handler;
