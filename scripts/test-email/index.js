// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for test-email
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  const { target, name, message } = ctx.data;
  const email = await ctx.moco.email.sendWithTemplate("test-email", target, {
    name: name,
    message: message,
  });
  return {
    data: email,
    error: null,
  };
}

module.exports = handler;
