// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for lihat-buku-penerbit
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  const getPenerbit = await ctx.moco.tables.findOne({
    table: "penerbit",
    orderBy: [
      {
        order: "desc",
        column: "created_at",
      },
    ],
  });
  const setCache = await ctx.moco.cache.set("penerbit-baru", getPenerbit);
  const cacheValue = await ctx.moco.cache.get("penerbit-baru");
  return {
    data: cacheValue,
    error: null,
  };
}

module.exports = handler;
