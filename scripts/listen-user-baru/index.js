// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for listen-user-baru
 * @param {import('@mocobaas/server-sdk').EventContext} ctx
 *
 */
async function handler(ctx) {
  console.log("listen-user-baru");
  // const userTerbaru = await ctx.moco.tables.findOne({
  //   table: "users",
  //   orderBy: [
  //     {
  //       order: "desc",
  //       column: "created_at",
  //     },
  //   ],
  //   select: ["email", "name"],
  // });

  await ctx.moco.queue.add(
    "global.test-queue",
    { email: ctx.data.email },
    {
      delay: 30000,
      backoff: {
        type: "exponential",
        delay: 5000,
        attempts: 5,
      },
    }
  );

  const email = await ctx.moco.email.sendWithTemplate(
    "email-notif",
    ctx.data.email,
    { message: "terima kasih" }
  );
  console.log(email);

  // ==========================
  // const bukuTerbaru = await ctx.moco.tables.findOne({
  //   table: "buku",
  //   orderBy: [
  //     {
  //       order: "desc",
  //       column: "created_at",
  //     },
  //   ],
  // });

  // const emailBuku = await ctx.moco.email.sendWithTemplate(
  //   "email-buku-terbaru",
  //   userTerbaru.email,
  //   {
  //     message: `ini adalah buku baru berjudul ${bukuTerbaru.judul_buku}`,
  //     jumlah: `${bukuTerbaru.jumlah_buku}`,
  //   }
  // );

  // setTimeout(emailBuku, 30000);

  // console.log(emailBuku);
  // ====================
}

module.exports = handler;
