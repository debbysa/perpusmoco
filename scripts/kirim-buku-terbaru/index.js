// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for kirim-email
 * @param {import('@mocobaas/server-sdk').EventContext} ctx
 *
 */
async function handler(ctx) {
  const bukuTerbaru = await ctx.moco.tables.findOne({
    table: "buku",
    orderBy: [
      {
        order: "desc",
        column: "created_at",
      },
    ],
  });
  const userTerbaru = await ctx.moco.tables.findOne({
    table: "users",
    orderBy: [
      {
        order: "desc",
        column: "created_at",
      },
    ],
    select: ["email"],
  });

  const { email } = ctx.data;
  console.log(ctx.data.email);
  const emailKirim = await ctx.moco.email.sendWithTemplate(
    "email-buku-terbaru",
    email,
    {
      message: `ini adalah buku baru berjudul ${bukuTerbaru.judul_buku}`,
      jumlah: `${bukuTerbaru.jumlah_buku}`,
    }
  );
  console.log(emailKirim);

  console.log("kirim-buku-terbaru");
}

module.exports = handler;
