// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for lihat-cache
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  const cacheValue = await ctx.moco.cache.get("data-user-baru");
  // console.log(cacheValue.valueOf());
  return {
    data: cacheValue,
    error: null,
  };
}
module.exports = handler;
