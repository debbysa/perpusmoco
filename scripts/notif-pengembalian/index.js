// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for notif-pengembalian
 * @param {import('@mocobaas/server-sdk').SchedulerContext} ctx
 *
 */
async function handler(ctx) {
  const moment = require("moment");
  const now = moment().format();
  const getPeminjaman = await ctx.moco.tables.findAll({
    table: "peminjaman",
    orderBy: [
      {
        order: "desc",
        column: "created_at",
      },
    ],
    include: ["users"],
  });
  for (let i = 0; i < getPeminjaman.count; i++) {
    let tanggal_pengembalian = getPeminjaman.results[i].tanggal_pengembalian;
    console.log(tanggal_pengembalian);
    const dateLine = moment(now).isSame(tanggal_pengembalian, "date");
    const isBefore = moment(now).isBefore(tanggal_pengembalian);
    console.log(getPeminjaman.results[i].id_peminjam);
    // const getPeminjam = await ctx.moco.tables.findById({
    //   table: "users",
    //   id: getPeminjaman.results[i].id_peminjam,
    // });
    // console.log(getPeminjam.email, getPeminjam.name);
    console.log(getPeminjaman.results[i].users.email);
    if (dateLine === true) {
      await ctx.moco.email.sendWithTemplate(
        "email.kembalikanBuku",
        getPeminjaman.results[i].users.email,
        {
          name: getPeminjaman.results[i].users.name,
          message: "anda harus segera mengembalikan buku",
        }
      );
    } else if (isBefore === true) {
      await ctx.moco.email.sendWithTemplate(
        "email.kembalikanBuku",
        getPeminjaman.results[i].users.email,
        {
          name: getPeminjaman.results[i].users.name,
          message: "Besok anda harus segera mengembalikan buku",
        }
      );
    }
  }
  console.log("notif-pengembalian");
}

module.exports = handler;
