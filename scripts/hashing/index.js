// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

const crypto = require("crypto");

/**
 *
 * @description handler for hashing
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  const hash = crypto
    .createHash("sha512")
    .update(ctx.data.plain)
    .digest("base64");

  return {
    data: { plain: ctx.data.plain, hash },
    error: null,
  };
}

module.exports = handler;
