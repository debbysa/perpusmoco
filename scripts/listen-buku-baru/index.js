// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for listen-buku-baru
 * @param {import('@mocobaas/server-sdk').EventContext} ctx
 *
 */
async function handler(ctx) {
  await ctx.moco.cache.set("data-buku-baru", ctx.data);
  console.log("listen-buku-baru");
}

module.exports = handler;
