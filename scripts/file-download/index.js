// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for file-download
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  const { localFilePath, filePath } = ctx.data;
  const file = await ctx.moco.file.retrieveFile(
    localFilePath,
    filePath,
    "learn-baas"
  );
  if (file.ok) {
    return {
      data: file,
    };
  } else {
    return {
      data: null,
      error: file.error,
    };
  }
}

module.exports = handler;
