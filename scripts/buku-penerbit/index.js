// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 *
 * @description handler for buku-penerbit-baru
 * @param {import('@mocobaas/server-sdk').ctx} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').returnCtx>}
 *
 */
async function handler(ctx) {
  // nama penerbit, nama penulis, alamat penulis, judul buku, jumlah buku -> ctx.data
  // 1. transaction Object
  const trxObj = await ctx.moco.tables.createTransaction();
  // 2. tambah penerbit
  const dataPenerbit = await ctx.moco.tables.create({
    table: "penerbit",
    data: {
      nama_penerbit: ctx.data.namaPenerbit,
      alamat_penerbit: ctx.data.alamatPenerbit,
    },
    transaction: trxObj,
  });
  // tambah penulis
  const dataPenulis = await ctx.moco.tables.create({
    table: "penulis",
    data: {
      nama_penulis: ctx.data.namaPenulis,
      alamat_penulis: ctx.data.alamatPenulis,
    },
  });
  // 3. tambah data buku
  await ctx.moco.tables.create({
    table: "buku",
    data: {
      judul_buku: ctx.data.judulBuku,
      jumlah_buku: ctx.data.jumlahBuku,
      id_penerbit: dataPenerbit.id,
      id_penulis: dataPenulis.id,
    },
    transaction: trxObj,
  });
  // 4. commit transaction
  await trxObj.commit();

  return {
    data: "buku-penerbit-baru",
    error: null,
  };
}

module.exports = handler;
