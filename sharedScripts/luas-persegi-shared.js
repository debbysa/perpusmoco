// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

async function luasPersegiShared(sisi) {
  const luasPersegi = sisi * sisi;
  return luasPersegi;
}

module.exports = luasPersegiShared;
